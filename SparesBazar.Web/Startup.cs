﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SparesBazar.Web.Startup))]
namespace SparesBazar.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
